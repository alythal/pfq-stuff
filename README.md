# Files in this directory

- `pfq-main.js` The blocked users script as it is used on PFQ. The version on PFQ is not a direct copy of the original in this repository

# Block Users Script - Features

# Install / Upgrade

Install the Tampermonkey extension for Firefox or Chrome. Then open [this link](https://gitlab.com/alythal/pfq-stuff/-/raw/master/blockFeature.user.js) to install the script. A Tampermonkey window should pop up asking you whether you want to install or upgrade the script

# Screenshots

I'm using my own profile (DrWho) as the example.

### Block a user

You can block a user from their profile.

![](./images/block_button.png)

### Unblock a user

Visit their profile. When it asks you to show the blocked profile, click Show. Then unblock the user in the user options.
![](./images/unblock.png)

## User profile

The profile is blocked, you can click 'Show' to show it anyway.
![](./images/block_user_profile.png)

## Multiuser profile

The name changes to 'Someone'. The trainer card and about me section are removed.
![](./images/block_users.png)

## Forum posts

Forum posts from blocked users are blocked, you can click Show to show them anyway.
![](./images/block_forum.png)

Quotes from blocked users are blocked as well

![](./images/block_quote.png)

## Other features (screenshots TBA)

- Option to anonymise users in clickback list or remove them entirely
- Nice!-s, PM and trade notifications from blocked users are not shown
- Anonymise links to blocked users anywhere they appear on PFQ
- Anonymise avatars of blocked users
