// ==UserScript==
// @name         PFQ Block users
// @namespace    http://tampermonkey.net/
// @version      0.0.9
// @description  Block users on PFQ
// @author       PFQ user DrWho
// @match        https://pokefarm.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

/**
 * @typedef {{
 *   count: string,
 *   currency: number[],
 *   html: string,
 *   nice: string[],
 *   ok: true
 * }} NotificationResponse
 * @typedef {{
 *   count: number[],
 *   currency: number[],
 *   dom: HTMLDivElement,
 *   nice: string[]
 * }} NotificationMutable
 */
$(function () {
	("use strict");
	var BLOCKED_USERS;

	const userlink = "a[href^='/user/']";

	const BLOCKED_MULTIUSER_PREF = {
		anonymise: "anonymise",
		remove: "remove",
		get: () => localStorage.getItem("BLOCKED_MULTIUSER_PREF") || "anonymise",
		set: (val) => localStorage.setItem("BLOCKED_MULTIUSER_PREF", val),
	};

	if (!$.pfqHook) $.pfqHook = {};
	const oldhook = $.pfqHook.notifications;
	/**
	 * Set up PFQ notifications hook
	 * @param {NotificationResponse} r
	 */
	$.pfqHook.notifications = (r) => {
		oldhook && oldhook(r);
		const tmp = document.createElement("div");
		tmp.innerHTML = r.html;
		r.dom = tmp;
		r.count = r.count.split("-"); // staff view is 1-2 where the numbers are user notifications and staff notifications
		blockNotifications(r);
		blockNices(r);
		redactAllLinks(r.dom);
		cleanUp(r);
		r.count = r.count.join("-");
		r.html = tmp.innerHTML;
		delete r.dom;
	};

	/**
	 * Remove headers from empty sections
	 * @param {NotificationMutable} r
	 */
	const cleanUp = function (r) {
		// remove headlines for empty sections
		Array.from(r.dom.querySelectorAll("ul.small"))
			.filter((e) => e.childElementCount < 1)
			.forEach((e) => removeElement(e.previousSibling));
		// add placeholder when there are no notifs
		if (!r.dom.querySelector("li")) {
			r.dom.innerHTML = `<p class="center"><i>There are no notices at this time.</i></p>`;
		}
	};

	/**
	 * Hide blocked PMs from the count, and censor links
	 * @param {NotificationMutable} r
	 */
	const blockNotifications = function (r) {
		const pms = r.dom.querySelectorAll(
			"[data-id^='pm-'], [data-id^='trade-received-']"
		);
		pms.forEach((pm) => {
			if (isBlocked(pm.querySelector("a[href^='/user/']"))) {
				r.count[0]--;
				removeElement(pm);
			}
		});
	};

	/**
	 * Removes blocked users from r.nice so no Nice popup will be triggered
	 *
	 * @param {NotificationMutable} r
	 */
	const blockNices = function (r) {
		r.nice = r.nice.filter(function (uname) {
			return !isBlocked(uname);
		});
	};

	/**
	 * Normalizes the string. Removes whitespace, turns it to lower case and
	 * converts non-latin letters to their closest latin counterpart.
	 *
	 * clean(" fÖø  bÄr ") == "foobar"
	 */
	const clean = function (s) {
		let r = s.trim().toLowerCase();
		r = r.replace(/(%20|[_ \s])/g, "");
		r = r.replace(new RegExp("[àáâãäå]", "g"), "a");
		r = r.replace(new RegExp("æ", "g"), "ae");
		r = r.replace(new RegExp("ç", "g"), "c");
		r = r.replace(new RegExp("[èéêë]", "g"), "e");
		r = r.replace(new RegExp("[ìíîï]", "g"), "i");
		r = r.replace(new RegExp("ñ", "g"), "n");
		r = r.replace(new RegExp("[òóôõöø]", "g"), "o");
		r = r.replace(new RegExp("œ", "g"), "oe");
		r = r.replace(new RegExp("[ùúûü]", "g"), "u");
		r = r.replace(new RegExp("[ýÿ]", "g"), "y");
		return r;
	};

	///////////////////////////////////////////////////
	// Interface to save, get and modify BLOCKED_USERS
	///////////////////////////////////////////////////
	const setBlockedUsers = function (val) {
		let v = JSON.stringify(val.map(clean));
		localStorage.setItem($.USERID + ".BLOCKED_USERS", v);
	};

	const getBlockedUsers = function () {
		let rv = localStorage.getItem($.USERID + ".BLOCKED_USERS");
		if (rv) {
			rv = JSON.parse(rv);
		} else {
			rv = [];
		}
		return rv.map(clean);
	};

	const block = function (uname) {
		if (!isBlocked(uname)) {
			// prevent blocking of staff
			let tcrank = document.querySelector("#trainercard .tc-header");
			if (!tcrank || tcrank.textContent != "Trainer") {
				alert("Staff cannot be blocked.");
				return false;
			} else {
				BLOCKED_USERS.push(uname);
				setBlockedUsers(BLOCKED_USERS);
				return true;
			}
		}
	};

	const unblock = function (uname) {
		let newval = BLOCKED_USERS.filter((s) => s !== clean(uname));
		setBlockedUsers(newval);
		return true;
	};

	/**
	 * Checks if the user is blocked. The comparison is made between the
	 * noramlized usernames */
	const isBlocked = function (username) {
		if (username.href && username.href.includes("/user/")) {
			username = username.href.split("/").slice(-1)[0];
		}
		if (username.dataset && username.dataset.name) {
			username = username.dataset.name;
		}
		return BLOCKED_USERS.includes(clean(username));
	};

	const blockOrUnblock = function (uname) {
		return isBlocked(uname) ? unblock(uname) : block(uname);
	};

	/////////////////////////////
	// /user Single profile page
	/////////////////////////////
	const onReady_user = async function () {
		let name = getName();
		addBlockButton();
		if (isBlocked(name)) {
			redactText(document.querySelector("title"));
			redactElement(
				document.querySelector("#content"),
				`You've blocked this profile.`
			);
		}
	};

	const getName = function () {
		return window.location.pathname.split("/").slice(-1)[0];
	};

	const addBlockButton = function (name) {
		let menu = document.querySelector("#aboutme > .menu");
		menu && menu.appendChild(blockButton(name));
	};

	const blockButton = function () {
		let name = getName();
		let blocked = isBlocked(name);
		let a = document.createElement("a");
		a.textContent = blocked ? `Remove block on ${name}` : `Block ${name}`;
		a.addEventListener("click", (e) => {
			e.preventDefault();
			e.stopPropagation();
			if (blockOrUnblock(name)) {
				refresh();
			}
		});
		return a;
	};

	// https://stackoverflow.com/a/9541579
	const isOverflown = function (element) {
		return (
			element.scrollHeight > element.clientHeight ||
			element.scrollWidth > element.clientWidth
		);
	};

	const fixTrainerCard = function () {
		let status = document.querySelector(".tc-status");
		if (status && isOverflown(status)) {
			status.style.fontSize = "8pt";
		}
	};

	/////////////////////////////
	// /users Multiuser page
	/////////////////////////////
	const onReady_users = async function () {
		if (BLOCKED_MULTIUSER_PREF.get() === BLOCKED_MULTIUSER_PREF.anonymise) {
			onUsersAnonymise();
		} else {
			onUsersRemove();
		}
	};

	/**
	 * Removes blocked users on multi-profile pages
	 */
	const onUsersRemove = function () {
		const usersOld = window.location.pathname.replace("/users/", "").split(",");
		const users = usersOld.filter((e) => !isBlocked(e));

		if (!users.length) {
			window.location = "/user/~me";
			return;
		}

		if (users.length < usersOld.length) {
			window.location = "/users/" + users.join(",");
		}
	};

	/**
	 * Anonymises blocked users on multi-profile pages
	 */
	const onUsersAnonymise = function () {
		const targetNode = document.querySelector("#multiuser");
		const config = { childList: true, subtree: true };
		const observer = new MutationObserver(() => {
			redactLabels();
			redactProfile();
		});
		observer.observe(targetNode, config);
	};

	const redactLabels = function () {
		Array.from(document.querySelectorAll("#multiuser label"))
			.filter((e) => isBlocked(e.textContent))
			.forEach(redactText);
	};

	const redactProfile = function () {
		let active = document.querySelector("div.tab-active");
		if (active) {
			let uname = active.querySelector("span.uname a");
			if (uname && isBlocked(uname.textContent)) {
				redactText(uname);
				hideElement(active.querySelector("#profilebox"));
			}
		}
	};

	/////////////////////////////
	// /forum Forum threads
	/////////////////////////////
	const onReady_forum = async function () {
		blockPosts();
		blockQuotes();
		blockThreads();
	};

	const blockPosts = function () {
		Array.from(
			document.querySelectorAll("div.forumpost:not(#preview)")
		).forEach(function (e) {
			let author = e.querySelector(".user > a");
			if (author && isBlocked(author)) {
				redactElement(e, "1 post blocked.", "forumpost");
				Array.from(e.querySelectorAll("img.avatar")).forEach(redactAvatar);
			}
		});
	};

	const redactAvatar = function (img) {
		img.src = `/img/unknown_trainer.png`;
	};

	const blockQuotes = function () {
		return Array.from(document.querySelectorAll(".bbcode .panel")).filter(
			function (e) {
				let header = e.querySelector("h3:first-child");
				if (header && header.textContent) {
					let isQuote = header.textContent.startsWith("QUOTE");
					if (isQuote) {
						let author = header.textContent.split(/\s/).slice(-1)[0];
						if (isBlocked(author)) {
							redactElement(e, "1 quote blocked", "panel");
						}
					}
				}
			}
		);
	};

	const blockThreads = function () {
		Array.from(document.querySelectorAll(".forumthreads > ul > li")).forEach(
			function (e) {
				let author = e.querySelector(`span.small > ${userlink}`);
				if (author && isBlocked(author)) {
					redactElement(e, "1 thread blocked", null, "li");
				}
			}
		);
	};

	/////////////////////////////
	// /pm Private Messages
	/////////////////////////////
	const onReady_pm = async function () {
		const targetNode = document.querySelector("#pmconvolist");
		// subtree must be false, this MO must not observe the children of #pmconvolist
		const config = { childList: true };
		const observer = new MutationObserver((mutations) => {
			if (mutations.length == 1 && mutations[0].addedNodes.length > 0) {
				blockMessages();
			}
		});
		observer.observe(targetNode, config);
	};

	const blockMessages = function () {
		let rows = Array.from(document.querySelectorAll("#pmconvolist > div"));
		if (rows.length < 1) {
			return;
		}
		rows.forEach(function (elem) {
			let uname = elem.querySelector && elem.querySelector("span > a");
			if (uname && isBlocked(uname)) {
				elem.style.display = "none";
			}
		});
	};

	/////////////////////////////
	// Any page
	/////////////////////////////
	const onReady_any = async function () {
		redactAllLinks(document);
		const targetNode = document.querySelector("#content");
		const config = { childList: true, subtree: true };
		const observer = new MutationObserver(function (mutations) {
			for (let m of mutations) {
				for (let node of m.addedNodes) {
					redactAllLinks(node);
				}
			}
		});
		observer.observe(targetNode, config);
	};

	const redactAllLinks = function (root = document) {
		if (root.querySelectorAll) {
			Array.from(root.querySelectorAll(userlink))
				.filter((a) => isBlocked(a))
				.forEach((a) => redactText(a));
		}
	};

	/////////////////////////////
	// Farm page
	/////////////////////////////
	const onReady_farm = async function () {
		renderSelection();
	};

	const unblockBulkByIndex = function (toUnblock) {
		// Pass an array of indices to remove from  BLOCKED_USERS and save the result
		// toUnblock: Array<number>
		let users = getBlockedUsers();
		let newval = users.filter((e, i) => !toUnblock.includes(i));
		setBlockedUsers(newval);
	};

	const renderSelection = function () {
		let users = getBlockedUsers();
		console.log(users);
		let html = `<div class='panel' style='margin: 8px 0;'  id='blocked-users-panel'><h3>Edit Blocked Users</h3><div>`;
		html += "<h3>On multiuser pages</h3><br>";
		html +=
			'<label><input type="radio" value="anonymise" name="multiuserBlock"> Anonymise the profile</label><br><br>';
		html +=
			'<label><input type="radio" value="remove" name="multiuserBlock"> Remove the profile</label><br><br>';
		html += "<hr>";
		html += "<h3>Edit blocked users</h3><br>";
		let body = users
			.map(function (e, i) {
				return `<label style='display: block; padding-bottom:1em'><input class='blocked-user-checkbox' data-i='${i}' type='checkbox' checked/> ${e}</label>`;
			})
			.join("");
		body = body || `No users blocked.`;
		body += `<br><br><button id="blocked-users-save">Save</button>`;
		html += body;
		html += `</div></div>`;
		let tmp = document.createElement("div");
		tmp.innerHTML = html;

		// select preference
		let pref = BLOCKED_MULTIUSER_PREF.get();
		tmp.querySelector(`input[value=${pref}]`).checked = true;

		let btn = tmp.querySelector("#blocked-users-save");
		if (btn) {
			btn.addEventListener("click", (e) => {
				let pref = document.querySelector(`input[name=multiuserBlock]:checked`)
					.value;

				BLOCKED_MULTIUSER_PREF.set(pref);
				let toRemove = Array.from(
					tmp.querySelectorAll(".blocked-user-checkbox:not(:checked)")
				).map((e) => parseInt(e.dataset.i, 10));
				unblockBulkByIndex(toRemove);
				refresh();
			});
		}
		//let target = document.querySelector("#farmtabs");
		//target && target.parentNode && target.parentNode.appendChild(tmp);
		let targetlist = document.querySelectorAll("#userscripts label");
		for (let i = 0; i < targetlist.length; i++) {
			if (targetlist[i].textContent.trim() === "Block Users") {
				targetlist[i].parentNode.appendChild(tmp);
				break;
			}
		}
	};

	/////////////////////////////
	// Trade page
	/////////////////////////////
	const onReady_trade = async function () {
		let blockedTrades = getBlockedTrades();
		blockedTrades.forEach((e) => removeElement(e));
		reduceTradeCount(blockedTrades.length);
	};

	const reduceTradeCount = function (num) {
		if (num < 1) return;
		let bubble = document.querySelector("#tradeindex > li[data-count]");
		if (bubble) {
			bubble.dataset["count"] -= num;
		}
		let headline = document.querySelector(
			"#activetrades > .panel:not(.left) > h3"
		);
		if (headline) {
			let count = headline.textContent.match(/\d+/);
			if (count) {
				headline.textContent = headline.textContent
					.replace(count, (count -= num))
					.replace("(0)", "");
			}
		}
	};

	const getBlockedTrades = function () {
		let incoming = Array.from(
			document.querySelectorAll(
				"#activetrades > .panel:not(.left) .tradelist > li"
			)
		);
		let blockedTrades = incoming.filter(function (e) {
			let uname = e.querySelector(".head > a[href^='/user/']");
			return isBlocked(uname);
		});

		return blockedTrades;
	};

	/////////////////////////////
	// Shelter page
	/////////////////////////////
	console.log('the script is running')
	const onReady_shelter = async function () {
		// the shelter opens a dialog box when you select a pokemon/egg. this
		// dialog contains a link to the user who donated the egg/pokemon,
		// which must be redacted.
		//
		// the tricky part is that .dialog is added/removed as a child of
		// body, so you must observe when .dialog is added.
		//
		// then, the content inside .dialog is loaded asynchornously, so you
		// must also observe when the content inside .dialog is fully loaded
		const targetNode = document.querySelector("body");
		const config = { childList: true };
		const nestedObserver = new MutationObserver((_, observer) => {
			// this nested observer runs when the content inside .dialog is
			// fully loaded. we immediately disconnect the observer before 
			// modifying the content inside .dialog. the disconnect is a must
			// to prevent an infinite loop.
			observer.disconnect()
			redactAllLinks(document.querySelector('.dialog'))
		})
		const observer = new MutationObserver((mutations) => {
			// .dialog was added to the page
			const mutation = mutations[0]
			if (!mutation.addedNodes) return
			const dialog = mutation.addedNodes[0]
			if (!dialog) return
			// start observing .dialog
			nestedObserver.observe(dialog, { childList: true, subtree: true })
		});
		observer.observe(targetNode, config);
	}

	
	/////////////////////////////
	// Helpers
	/////////////////////////////
	/**
	 * Remove an element from the DOM
	 *
	 * @param {Node} elem Element to Remove
	 */
	const removeElement = function (elem) {
		return elem.parentNode.removeChild(elem);
	};

	/**
	 * Change the innerHTML of a node to 'Someone'
	 * @param {Node} elem Element to alter
	 */
	const redactText = function (elem) {
		elem.innerHTML = "Someone";
	};

	const redactElement = function (elem, text, cls, node = "div") {
		elem.style.display = "none";
		let div = document.createElement(node);
		div.textContent = text + " ";
		div.style.padding = "1em";
		if (cls) {
			div.classList.add(cls);
		}
		let btn = document.createElement("i");
		btn.style.textDecoration = "underline";
		btn.textContent = "Show";
		btn.onclick = function () {
			div.style.display = "none";
			elem.style.display = "";
			try {
				fixTrainerCard();
			} catch (e) {
				console.log(e);
			}
		};
		div.appendChild(btn);
		elem.parentNode.insertBefore(div, elem);
		return div;
	};

	const hideElement = function (elem) {
		elem.style.opacity = "0";
	};

	const refresh = function () {
		window.location.href = window.location.origin + window.location.pathname;
	};

	/////////////////////////////
	// Main function
	/////////////////////////////
	const handlers = [
		{
			startsWith: "/forum",
			handler: onReady_forum,
		},
		{
			startsWith: "/users/",
			handler: onReady_users,
		},
		{
			startsWith: "/user/",
			handler: onReady_user,
		},
		{
			startsWith: "/pm",
			handler: onReady_pm,
		},
		{
			startsWith: "/farm",
			handler: onReady_farm,
		},
		{
			startsWith: "/trade",
			handler: onReady_trade,
		},
		{
			startsWith: "/shelter",
			handler: onReady_shelter
		},
		{
			startsWith: "",
			handler: onReady_any,
		},
	];

	BLOCKED_USERS = getBlockedUsers();
	let loc = window.location.pathname;
	handlers
		.filter((h) => loc.startsWith(h.startsWith))
		.forEach((h) => h.handler());
});
