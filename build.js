const esbuild = require("esbuild");

esbuild.buildSync({
	outdir: "./dist",
	watch: true,
	entryPoints: ["./src/blockUser.ts"],
	bundle: true,
});
