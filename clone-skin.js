// ==UserScript==
// @name         PFQ Clone current skin
// @namespace    https://pokefarm.com/user/DrWho
// @version      0.1
// @description  The problem: Imagine you're using another user's skin and you love it but want to change 1 small thing.
//     You can't edit another user's skin though! This script adds a button to the page where you create a new skin.
//     Click the button to copy the values of the skin you're currently using into the form. You'll have an exact clone
//     of the skin and it's yours to edit as you like.
// @author       https://pokefarm.com/user/DrWho
// @match        https://pokefarm.com/skin/edit
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function () {
	"use strict";

	function getResourcePaths(pathToCss) {
		let base = pathToCss.match(/(\/skins\/user\/.+?)index\/sally.css/i);
		if (!base) {
			throw new Error("Invalid skin location " + pathToCss);
		}
		base = base[1];
		let rv = {
			colours: `${base}__colours.less`,
			extra: `${base}__extra.less`,
		};
		return rv;
	}

	async function getText(location) {
		let res = await fetch(location);
		if (res.status >= 400) {
			throw new Error(
				`Could not fetch!\nLocation: ${location}\nResponse: ${JSON.stringify(
					response
				)}`
			);
		}
		return await res.text();
	}

	function processColorsFile(colorsLess) {
		let rv = {};
		let keyValueRegex = /^@(.+?):\s+(.+?);$/m;
		colorsLess
			.split("\n")
			.map((line) => line.match(keyValueRegex))
			.filter((id) => id)
			.filter((groups) => !groups[2].includes("@")) //drop if links set automatically with reference
			.map((matchrv) => [matchrv[1], matchrv[2]]) //map to captures
			.map(function (group) {
				let key = group[0].replace(/-/g, "_");
				return {
					[key]: group[1],
				};
			})
			.forEach((obj) => Object.assign(rv, obj));
		return rv;
	}

	/**
	 * Convert the value to something suitable for the input
	 * box. Currently only used to convert
	 * url("img.png") -> img.png
	 */
	function convertToInputValue(value) {
		const re = /url\(['"](.+)["']\)/;
		let matcher = value.match(re);
		if (matcher) {
			return matcher[1];
		}
		return value;
	}

	async function main() {
		try {
			let paths = getResourcePaths(document.styleSheets[0].href);
			let inputs = Array.from(
				document.querySelectorAll("#skineditorform input[name]")
			);
			//Write resource paths into names
			let declarations = processColorsFile(await getText(paths.colours));
			for (let key of Object.keys(declarations)) {
				//find proper input field
				let input = inputs.find((inp) => inp.name === key);
				if (!input) {
					continue;
				}
				//write value
				input.value = convertToInputValue(declarations[key]);
				//trigger preview update
				input.dispatchEvent(new Event("keyup", { bubbles: true }));
			}
			//Write extra css
			let extraCssInput = document.querySelector('textarea[name="extracss"]');
			extraCssInput.value = await getText(paths.extra);
		} catch (err) {
			alert(
				"An error occurred :( Plese check the browser console for details [Ctrl+Shift+i] if you're on desktop"
			);
			console.error(err);
		}
	}

	function init() {
		let button = document.createElement("button");
		button.textContent = "Copy current skin";
		let target = document.querySelector("#skineditorform > .panel");
		button.addEventListener("click", main);
		target.parentNode.insertBefore(button, target);
	}

	init();
})();
