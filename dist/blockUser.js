(() => {
  // src/notepadHandle.ts
  function parse(text) {
    let doc = new DOMParser().parseFromString(text, "text/html");
    return doc;
  }
  var NotepadHandle = class {
    constructor(filename) {
      this.filename = filename;
      this.id = "";
    }
    static async createHandle(filename) {
      let handle = new NotepadHandle(filename);
      await handle.init();
      return handle;
    }
    async init() {
      let id = await this.getId();
      if (id) {
        this.id = id;
      } else {
        this.id = await this.create();
      }
    }
    async getId() {
      let res = await fetch("https://pokefarm.com/farm/notepad", {
        headers: {
          "X-Requested-With": "Love"
        },
        body: '{"directory":null}',
        method: "POST"
      });
      let doc = parse((await res.json()).html);
      let file = Array.from(doc.querySelectorAll("a")).filter((e) => e.textContent === this.filename);
      return file.pop()?.dataset?.file ?? null;
    }
    async create() {
      let res = await fetch("/farm/notepad", {
        headers: {
          "X-Requested-With": "Love"
        },
        body: JSON.stringify({
          directory: "",
          mode: "newfile",
          save: {
            name: this.filename
          }
        }),
        method: "POST"
      });
      let doc = parse((await res.json()).html);
      let id = doc.querySelector("form")?.dataset?.fileform;
      if (id) {
        return id;
      }
      throw new Error(`Failed to create notepad file`);
    }
    async read() {
      let res = await fetch("/farm/notepad", {
        headers: {
          "X-Requested-With": "Love"
        },
        body: JSON.stringify({
          directory: null,
          file: this.id
        }),
        method: "POST"
      });
      let doc = parse((await res.json()).html);
      let value = doc.querySelector(`textarea[name='content']`)?.value;
      if (value) {
        return value;
      }
      throw new Error(`Failed to create notepad file`);
    }
    async write(content) {
      let res = await fetch("/farm/notepad", {
        headers: {
          "X-Requested-With": "Love"
        },
        body: JSON.stringify({
          file: this.id,
          mode: "save",
          save: {
            content,
            name: this.filename
          }
        }),
        method: "POST"
      });
      if (res.status !== 200) {
        throw Error(`Could not save to notepad file`);
      }
    }
  };
  var notepadHandle_default = NotepadHandle;

  // src/store.ts
  var Store = class {
    constructor(handle, storageName) {
      this.handle = handle;
      this.storageName = storageName;
    }
    static async createStore(userScriptName, initialState2) {
      const storageName = `${window.$.USERID}-${userScriptName}`;
      const handle = await notepadHandle_default.createHandle(storageName);
      let store2 = new Store(handle, storageName);
      await store2.init(initialState2);
      return store2;
    }
    cacheExists() {
      return localStorage.getItem(this.storageName) !== null;
    }
    stringify(state) {
      return JSON.stringify(state);
    }
    parse(s) {
      return JSON.parse(s);
    }
    async init(initialState2) {
      const updateIfNeeded = async () => {
        let state = this.get();
        if (state.version < initialState2.version) {
          let merged = {
            ...initialState2,
            ...state,
            version: initialState2.version
          };
          await this.set(merged);
        }
      };
      if (this.cacheExists()) {
        await updateIfNeeded();
        return;
      }
      let read = await this.handle.read();
      if (read) {
        await this._setString(read);
        await updateIfNeeded();
        return;
      }
      let s = JSON.stringify(initialState2);
      this.handle.write(s);
      this._setString(s);
    }
    get() {
      return this.parse(localStorage.getItem(this.storageName));
    }
    async set(state) {
      let s = this.stringify(state);
      return await this._setString(s);
    }
    async _setString(s) {
      localStorage.setItem(this.storageName, s);
      return await this.handle.write(s);
    }
  };
  var store_default = Store;

  // src/blockUser.ts
  var initialState = {
    version: 1,
    blockedUsers: [],
    preference: "anonymise"
  };
  var store = store_default.createStore("blockUsers", initialState);
})();
