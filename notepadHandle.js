function parse(text) {
	let doc = new DOMParser().parseFromString(text, "text/html");
	return doc;
}

/**
 * Class to manage a file in the the PFQ Notepad. Currently only
 * supports file in the root directory
 *
 * let handle = await NotepadHandle.createHandle("boop")
 * handle.read()                // returns the currentfile content
 * handle.write('hello world') // write to the file
 */
class NotepadHandle {
	static async createHandle(filename) {
		let handle = new NotepadHandle(filename);
		await handle.init();
		return handle;
	}

	constructor(filename) {
		this.filename = filename;
		this.id = null;
	}

	async init() {
		this.id = await this.getId();
		if (!this.id) {
			this.id = await this.create();
		}
	}

	/* Returns the ID if document exists, otherwise null */
	async getId() {
		// Fetch the notepad page
		let res = await fetch("https://pokefarm.com/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: '{"directory":null}',
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		// Finds the element matching the example below and returns the file ID
		// <a data-file='12345'>this.filename</a>
		let file = Array.from(doc.querySelectorAll("a")).filter(
			(e) => e.textContent === this.filename
		);
		if (file.length) {
			let id = file.pop().dataset.file;
			return id;
		}
		return null;
	}

	async create() {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				directory: "",
				mode: "newfile",
				save: {
					name: this.filename,
				},
			}),
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		let id = doc.querySelector("form").dataset.fileform;
		return id;
	}

	async read() {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				directory: null,
				file: this.id,
			}),
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		let value = doc.querySelector(`textarea[name='content']`).value;
		return value;
	}

	async write(content) {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				file: this.id,
				mode: "save",
				save: {
					content,
					name: this.filename,
				},
			}),
			method: "POST",
		});
		if (res.status !== 200) {
			throw Error(`Could not save content! ${this}`);
		}
	}
}
