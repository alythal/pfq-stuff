/**
 * @typedef {{
 *   count: string,
 *   currency: number[],
 *   html: string,
 *   nice: string[],
 *   ok: true
 * }} NotificationResponse
 * @typedef {{
 *   count: number[],
 *   currency: number[],
 *   dom: HTMLDivElement,
 *   nice: string[]
 * }} NotificationMutable
 */
$(function () {
	"use strict";
	var BLOCKED_USERS;

	if (!$.pfqHook) $.pfqHook = {};
	const oldhook = $.pfqHook.notifications;
	/**
	 * Set up PFQ notifications hook
	 * @param {NotificationResponse} r
	 */
	$.pfqHook.notifications = (r) => {
		oldhook && oldhook(r);
		const tmp = document.createElement("div");
		tmp.innerHTML = r.html;
		r.dom = tmp;
		r.count = r.count.split("-"); // staff view is 1-2 where the numbers are user notifications and staff notifications
		blockNotifications(r);
		blockNices(r);
		redactAllLinks(r.dom);
		cleanUp(r);
		r.count = r.count.join("-");
		r.html = tmp.innerHTML;
		delete r.dom;
	};
	/**
	 * Remove headers from empty sections
	 * @param {NotificationMutable} r
	 */
	const cleanUp = function (r) {
		// remove headlines for empty sections
		Array.from(r.dom.querySelectorAll("ul.small"))
			.filter((e) => e.childElementCount < 1)
			.forEach((e) => removeElement(e.previousSibling));
		// add placeholder when there are no notifs
		if (!r.dom.querySelector("li")) {
			r.dom.innerHTML = `<p class="center"><i>There are no notices at this time.</i></p>`;
		}
	};

	/**
	 * Hide blocked PMs from the count, and censor links
	 * @param {NotificationMutable} r
	 */
	const blockNotifications = function (r) {
		const pms = r.dom.querySelectorAll(
			"[data-id^='pm-'], [data-id^='trade-received-']"
		);
		pms.forEach((pm) => {
			if (isBlocked(pm.querySelector("a[href^='/user/']").textContent)) {
				r.count[0]--;
			}
		});

		const userlinks = r.dom.querySelectorAll("a[href^='/user/']");
		userlinks.forEach((link) => {
			if (isBlocked(link.textContent)) redactText(link);
		});
	};
	/**
	 * Removes blocked users from r.nice so no Nice popup will be triggered
	 *
	 * @param {NotificationMutable} r
	 */
	const blockNices = function (r) {
		r.nice = r.nice.filter(function (uname) {
			return !isBlocked(uname);
		});
	};

	/**
	 * Normalizes the string. Removes whitespace, turns it to lower case and
	 * converts non-latin letters to their closest latin counterpart.
	 *
	 * clean(" fÃ–Ã¸  bÃ„r ") == "foobar"
	 */
	const clean = function (s) {
		let r = s.trim().toLowerCase();
		r = r.replace(/(%20|[_ \s])/g, "");
		r = r.replace(new RegExp("[Ã Ã¡Ã¢Ã£Ã¤Ã¥]", "g"), "a");
		r = r.replace(new RegExp("Ã¦", "g"), "ae");
		r = r.replace(new RegExp("Ã§", "g"), "c");
		r = r.replace(new RegExp("[Ã¨Ã©ÃªÃ«]", "g"), "e");
		r = r.replace(new RegExp("[Ã¬Ã­Ã®Ã¯]", "g"), "i");
		r = r.replace(new RegExp("Ã±", "g"), "n");
		r = r.replace(new RegExp("[Ã²Ã³Ã´ÃµÃ¶Ã¸]", "g"), "o");
		r = r.replace(new RegExp("Å“", "g"), "oe");
		r = r.replace(new RegExp("[Ã¹ÃºÃ»Ã¼]", "g"), "u");
		r = r.replace(new RegExp("[Ã½Ã¿]", "g"), "y");
		return r;
	};

	///////////////////////////////////////////////////
	// Interface to save, get and modify BLOCKED_USERS
	///////////////////////////////////////////////////
	const setBlockedUsers = function (val) {
		let v = JSON.stringify(val.map(clean));
		localStorage.setItem($.USERID + ".BLOCKED_USERS", v);
	};

	const getBlockedUsers = function () {
		let rv = localStorage.getItem($.USERID + ".BLOCKED_USERS");
		if (rv) {
			rv = JSON.parse(rv);
		} else {
			rv = [];
		}
		return rv.map(clean);
	};

	const block = function (uname) {
		if (!isBlocked(uname)) {
			// prevent blocking of staff
			let tcrank = document.querySelector("#trainercard .tc-header");
			if (!tcrank || tcrank.textContent != "Trainer") {
				alert("Staff cannot be blocked.");
				return false;
			} else {
				BLOCKED_USERS.push(uname);
				setBlockedUsers(BLOCKED_USERS);
				return true;
			}
		}
	};

	const unblock = function (uname) {
		let newval = BLOCKED_USERS.filter((s) => s !== clean(uname));
		setBlockedUsers(newval);
		return true;
	};

	/**
	 * Checks if the user is blocked. The comparison is made between the
	 * noramlized usernames */
	const isBlocked = function (username) {
		if (username.href && username.href.includes("/user/")) {
			username = username.href.split("/").slice(-1)[0];
		}
		if (username.dataset && username.dataset.name) {
			username = username.dataset.name;
		}

		return BLOCKED_USERS.includes(clean(username));
	};

	const blockOrUnblock = function (uname) {
		return isBlocked(uname) ? unblock(uname) : block(uname);
	};

	/////////////////////////////
	// /user Single profile page
	/////////////////////////////
	const onReady_user = function () {
		let name = getName();
		addBlockButton();
		if (isBlocked(name)) {
			redactText(document.querySelector("title"));
			redactElement(
				document.querySelector("#content"),
				`You've blocked this profile.`
			);
		}
	};

	const getName = function () {
		return window.location.pathname.split("/").slice(-1)[0];
	};

	const addBlockButton = function (name) {
		let menu = document.querySelector("#aboutme > .menu");
		menu && menu.appendChild(blockButton(name));
	};

	const blockButton = function () {
		let name = getName();
		let blocked = isBlocked(name);
		let a = document.createElement("a");
		a.textContent = blocked ? `Remove block on ${name}` : `Block ${name}`;
		a.addEventListener("click", (e) => {
			e.preventDefault();
			e.stopPropagation();
			if (blockOrUnblock(name)) {
				refresh();
			}
		});
		return a;
	};

	// https://stackoverflow.com/a/9541579
	const isOverflown = function (element) {
		return (
			element.scrollHeight > element.clientHeight ||
			element.scrollWidth > element.clientWidth
		);
	};

	const fixTrainerCard = function () {
		let status = document.querySelector(".tc-status");
		if (status && isOverflown(status)) {
			status.style.fontSize = "8pt";
		}
	};

	/////////////////////////////
	// /users Multiuser page
	/////////////////////////////
	const onReady_users = function (e) {
		const targetNode = document.querySelector("#multiuser");
		const config = { childList: true, subtree: true };
		const observer = new MutationObserver((mutations) => {
			redactLabels();
			redactProfile();
		});
		observer.observe(targetNode, config);
	};

	const redactLabels = function (e) {
		Array.from(document.querySelectorAll("#multiuser label"))
			.filter((e) => isBlocked(e.textContent))
			.forEach(redactText);
	};

	const redactProfile = function () {
		let active = document.querySelector("div.tab-active");
		if (active) {
			let uname = active.querySelector("span.uname a");
			if (uname && isBlocked(uname.textContent)) {
				redactText(uname);
				hideElement(active.querySelector("#profilebox"));
			}
		}
	};

	/////////////////////////////
	// /forum Forum threads
	/////////////////////////////
	const onReady_forum = function (e) {
		blockPosts();
		blockQuotes();
		blockThreads();
	};

	const blockPosts = function () {
		Array.from(
			document.querySelectorAll("div.forumpost:not(#preview)")
		).forEach(function (e) {
			let author = e.querySelector(".user > a");
			if (author && isBlocked(author.textContent)) {
				redactElement(e, "1 post blocked.", "forumpost");
			}
		});
	};

	const blockQuotes = function () {
		return Array.from(document.querySelectorAll(".bbcode .panel")).filter(
			function (e) {
				let header = e.querySelector("h3:first-child");
				if (header && header.textContent) {
					let isQuote = header.textContent.startsWith("QUOTE");
					if (isQuote) {
						let author = header.textContent.split(/\s/).slice(-1)[0];
						if (isBlocked(author)) {
							redactElement(e, "1 quote blocked", "panel");
						}
					}
				}
			}
		);
	};

	const blockThreads = function () {
		Array.from(document.querySelectorAll(".forumthreads > ul > li")).forEach(
			function (e) {
				let author = e.querySelector("span.small > a[href^='/user/']");
				if (author && isBlocked(author.textContent)) {
					redactElement(e, "1 thread blocked", null, "li");
				}
			}
		);
	};

	/////////////////////////////
	// /pm Private Messages
	/////////////////////////////
	const onReady_pm = async function () {
		const targetNode = document.querySelector("#pmconvolist");
		// subtree must be false, this MO must not observe the children of #pmconvolist
		const config = { childList: true };
		const observer = new MutationObserver((mutations) => {
			if (mutations.length == 1 && mutations[0].addedNodes.length > 0) {
				blockMessages();
			}
		});
		observer.observe(targetNode, config);
	};

	const blockMessages = function () {
		let rows = Array.from(document.querySelectorAll("#pmconvolist > div"));
		if (rows.length < 1) {
			return;
		}
		rows.forEach(function (elem) {
			let uname = elem.querySelector && elem.querySelector("span > a");
			if (uname && isBlocked(uname)) {
				elem.style.display = "none";
			}
		});
	};

	/////////////////////////////
	// Any page
	/////////////////////////////
	const onReady_any = async function () {
		redactAllLinks(document);
		const targetNode = document.querySelector("#content");
		const config = { childList: true, subtree: true };
		const observer = new MutationObserver(function (mutations) {
			for (let m of mutations) {
				for (let node of m.addedNodes) {
					redactAllLinks(node);
				}
			}
		});
		observer.observe(targetNode, config);
	};

	const redactAllLinks = function (root = document) {
		if (root.querySelectorAll) {
			Array.from(root.querySelectorAll("a[href^='/user/']"))
				.filter((a) => isBlocked(a.textContent))
				.forEach((a) => redactText(a));
		}
	};

	/////////////////////////////
	// Farm page
	/////////////////////////////
	const onReady_farm = async function () {
		renderSelection();
	};

	const unblockBulkByIndex = function (toUnblock) {
		// Pass an array of indices to remove from  BLOCKED_USERS and save the result
		// toUnblock: Array<number>
		let users = getBlockedUsers();
		let newval = users.filter((e, i) => !toUnblock.includes(i));
		setBlockedUsers(newval);
	};

	const renderSelection = function () {
		let users = getBlockedUsers();
		let html = `<div class='panel' style='margin: 8px 0;' id='blocked-users-panel'><h3>Edit Blocked Users</h3><div>`;
		let body = users
			.map(function (e, i) {
				return `<label style='display: block; padding-bottom:1em'><input class='blocked-user-checkbox' data-i='${i}' type='checkbox' checked/> ${e}</label>`;
			})
			.join("");
		body = body
			? body + `<button id='blocked-users-save'>Save</button>`
			: `No users blocked.`;
		html += body;
		html += `</div></div>`;
		let tmp = document.createElement("div");
		tmp.innerHTML = html;
		let btn = tmp.querySelector("#blocked-users-save");
		if (btn) {
			btn.addEventListener("click", (e) => {
				let toRemove = Array.from(
					tmp.querySelectorAll(".blocked-user-checkbox:not(:checked)")
				).map((e) => parseInt(e.dataset.i, 10));
				unblockBulkByIndex(toRemove);
				refresh();
			});
		}
		let targetlist = document.querySelectorAll("#userscripts label");
		for (let i = 0; i < targetlist.length; i++) {
			if (targetlist[i].textContent.trim() === "Block Users") {
				targetlist[i].parentNode.appendChild(tmp);
				break;
			}
		}
	};

	/////////////////////////////
	// Trade page
	/////////////////////////////
	const onReady_trade = async function () {
		let blockedTrades = getBlockedTrades();
		blockedTrades.forEach((e) => removeElement(e));
		reduceTradeCount(blockedTrades.length);
	};

	const reduceTradeCount = function (num) {
		if (num < 1) return;
		let bubble = document.querySelector("#tradeindex > li[data-count]");
		if (bubble) {
			bubble.dataset["count"] -= num;
		}
		let headline = document.querySelector(
			"#activetrades > .panel:not(.left) > h3"
		);
		if (headline) {
			let count = headline.textContent.match(/\d+/);
			if (count) {
				headline.textContent = headline.textContent
					.replace(count, (count -= num))
					.replace("(0)", "");
			}
		}
	};

	const getBlockedTrades = function () {
		let incoming = Array.from(
			document.querySelectorAll(
				"#activetrades > .panel:not(.left) .tradelist > li"
			)
		);
		let blockedTrades = incoming.filter(function (e) {
			let uname = e.querySelector(".head > a[href^='/user/']").textContent;
			return isBlocked(uname);
		});

		return blockedTrades;
	};

	/////////////////////////////
	// Helpers
	/////////////////////////////
	const redactText = function (elem) {
		elem.innerHTML = "Someone";
	};

	const redactElement = function (elem, text, cls, node = "div") {
		elem.style.display = "none";
		let div = document.createElement(node);
		div.textContent = text + " ";
		div.style.padding = "1em";
		if (cls) {
			div.classList.add(cls);
		}
		let btn = document.createElement("i");
		btn.style.textDecoration = "underline";
		btn.textContent = "Show";
		btn.onclick = function () {
			div.style.display = "none";
			elem.style.display = "";
			try {
				fixTrainerCard();
			} catch (e) {
				console.log(e);
			}
		};
		div.appendChild(btn);
		elem.parentNode.insertBefore(div, elem);
		return div;
	};

	const hideElement = function (elem) {
		elem.style.opacity = "0";
	};

	const refresh = function () {
		window.location.href = window.location.href;
	};

	/////////////////////////////
	// Main function
	/////////////////////////////
	const handlers = [
		{
			startsWith: "/forum",
			handler: onReady_forum,
		},
		{
			startsWith: "/users/",
			handler: onReady_users,
		},
		{
			startsWith: "/user/",
			handler: onReady_user,
		},
		{
			startsWith: "/pm",
			handler: onReady_pm,
		},
		{
			startsWith: "/farm",
			handler: onReady_farm,
		},
		{
			startsWith: "/trade",
			handler: onReady_trade,
		},
		{
			startsWith: "",
			handler: onReady_any,
		},
	];

	BLOCKED_USERS = getBlockedUsers();
	let loc = window.location.pathname;
	handlers
		.filter((h) => loc.startsWith(h.startsWith))
		.forEach((h) => h.handler());
});
