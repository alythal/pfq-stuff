// ==UserScript==
// @name         PKRS Notification
// @namespace    http://tampermonkey.net/
// @version      0.0.5
// @description  try to take over the world!
// @author       pokefarm.com/user/drwho
// @match        https://pokefarm.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function () {
  "use strict";

  function secondsElapsed(t) {
    let ms = Date.now() - t;
    let s = ms / 1000;
    return s;
  }

  function createInterface(key) {
    return {
      get: () => localStorage.getItem(key),
      set: () => localStorage.setItem(key, Date.now()),
      secondsElapsed: () => secondsElapsed(localStorage.getItem(key)),
    };
  }

  const lastInteraction = createInterface($.USERID + "__userscript_last_interaction");
  const lastNotification = createInterface($.USERID + "__userscript_last_notification");

  // set last interaction when page is loaded or user clicks
  lastInteraction.set();

  // run every 30 seconds
  const id = window.setInterval(checkForPokerus, 30000);

  async function hasPokerus() {
    // true if user is pkrs holder
    let me = await fetch("/user/~me", { method: "HEAD" });
    let pkrs = await fetch("/user/~pkrs", { method: "HEAD" });
    return me.url === pkrs.url;
  }

  async function checkForPokerus() {
    if (
      lastInteraction.secondsElapsed() > 60 &&
      lastNotification.secondsElapsed() > 60 &&
      (await hasPokerus())
    ) {
      sendNotification();
    }
  }

  function sendNotification() {
    // update title
    const prefix = "[PKRS]";
    if (!document.title.includes(prefix)) {
      document.title = `${prefix} ${document.title}`;
    }
    //browser notif
    new Notification("pokefarm.com", {
      body: "You have PKRS",
      icon: "https://pfq-static.com/img/notifications/niet.png",
    });
    lastNotification.set();
  }
})();
