// ==UserScript==
// @name         New Userscript
// @namespace    https://pokefarm.com/user/DrWho
// @version      0.1
// @description  Replace nature Lax with Lazy
// @author       https://pokefarm.com/user/DrWho
// @match        https://pokefarm.com/*
// @icon         https://pokefarm.com/favicon.ico
// @grant        none
// ==/UserScript==

(function() {
    const replaceNatureCb = () => Array.from(document.querySelectorAll('.nature > b')).filter(e => e.textContent === 'Lax').forEach(e => { e.textContent = 'Lazy' });
    // needed if dynamically inserted elements contain a pokemon's nature
    // new MutationObserver(replaceNatureCb).observe(document.querySelector('#content'), { childList: true, subtree: true });
    replaceNatureCb();
})();
