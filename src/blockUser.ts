import Store, { State } from "./store";

interface Config extends State {
	preference: "anonymise" | "remove";
	blockedUsers: string[];
}

const initialState: Config = {
	version: 1,
	blockedUsers: [],
	preference: "anonymise",
};

const store = Store.createStore("blockUsers", initialState);
