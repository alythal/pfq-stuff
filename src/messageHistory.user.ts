// ==UserScript==
// @name         PFQ Message History
// @namespace    http://pokefarm.com/user/drwho/
// @version      0.1
// @description  todo
// @author       user/drwho
// @match        https://pokefarm.com/pm/read/*
// @run-at       document-end
// @grant        none
// ==/UserScript==

(function () {
	"use strict";

	const $update = "update:messageCount";

	interface Message {
		displayName: string;
		date: string;
		content: string;
	}

	const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

	async function* createMessageGenerator(
		messageSelector: string,
		createMessage: (HTMLElement) => Message,
		urls: string[],
		delay: number = 1000
	) {
		for (let url of urls) {
			let res = await fetch(url, {
				credentials: "include",
			});
			let html = await res.text();
			let parser = new DOMParser();
			let doc = parser.parseFromString(html, "text/html");
			let messages = Array.from(doc.querySelectorAll(messageSelector)).map(
				createMessage
			);
			yield messages;
			await sleep(delay);
			document.dispatchEvent(
				new CustomEvent($update, { detail: messages.length })
			);
		}
	}

	const messageSelector = ".forumpost:not(#preview)";
	const createMessage = function (div: HTMLElement): Message {
		let fn: (string) => string = (selector) =>
			div.querySelector(selector).textContent;
		return {
			displayName: fn(".user > a"),
			date: fn(".metadata"),
			content: fn("div[id^=userpost_]"),
		};
	};

	const maxPageContainer = document.querySelector(
		".pagelist > [data-page]:last-of-type"
	);
	const maxPage = maxPageContainer
		? parseInt(maxPageContainer.getAttribute("data-page"), 10)
		: 1;

	const urls = (function () {
		let base = window.location.pathname.replace(/\/\d*$/, "");
		return Array<any>(maxPage)
			.fill(0)
			.map((e, i) => `${base}/${i + 1}`);
	})();

	async function collectMessages(): Promise<Message[]> {
		let messageGenerator = createMessageGenerator(
			messageSelector,
			createMessage,
			urls
		);
		let messages: Message[] = [];
		let res = await messageGenerator.next();
		while (!res.done) {
			messages = messages.concat(res.value || []);
			res = await messageGenerator.next();
		}
		return messages;
	}

	class MessageRepository {
		key: string;

		constructor(from: string, to: string) {
			this.key = `userscript_messagehistory_${from}_${to}`;
		}

		async get() {
			if (!localStorage.getItem(this.key)) {
				let msgs = await collectMessages();
				localStorage.setItem(this.key, JSON.stringify(msgs));
			}
			return JSON.parse(localStorage.getItem(this.key));
		}

		async set(message: Message[]) {
			localStorage.setItem(this.key, JSON.stringify(message));
		}
	}

	interface WordCount {
		word: string;
		count: number;
	}

	interface UserBagOfWords {
		displayName: string;
		bagOfWords: WordCount[];
	}

	function _unique<T>(arr: T[]) {
		return [...new Set(arr)];
	}

	function _countUnique(words: string[]): WordCount[] {
		let map: Map<string, number> = new Map();
		for (let w of words) {
			map.set(w, (map.get(w) || 0) + 1);
		}
		let rv = [...map.entries()]
			.map(([word, count]) => ({ word, count }))
			.sort((a, b) => b.count - a.count);
		return rv;
	}

	function processMessages(
		messages: Message[],
		tokenize: (s: string) => string[]
	): [UserBagOfWords, UserBagOfWords] {
		let uniqueNames = _unique(messages.map((e) => e.displayName));
		if (uniqueNames.length != 2) {
			throw Error(`Expected 2 unique names, got ${uniqueNames.length}`);
		}
		let words = (name) =>
			messages
				.filter((m) => m.displayName === name)
				.map((m) => tokenize(m.content))
				.reduce((acc, arr) => acc.concat(arr), []);
		let [name1, name2] = uniqueNames;
		let bagify = (name) => ({
			displayName: name,
			bagOfWords: _countUnique(words(name)),
		});
		return [bagify(name1), bagify(name2)];
	}

	// prettier-ignore
	// https://gist.github.com/sebleier/554280
	const stopWords = ["i","i'm","me","my","myself","we","our","ours","ourselves","you","your","yours","yourself","yourselves","he","him","his","himself","she","her","hers","herself","it","its","itself","they","them","their","theirs","themselves","what","which","who","whom","this","that","these","those","am","is","are","was","were","be","been","being","have","has","had","having","do","does","did","doing","a","an","the","and","but","if","or","because","as","until","while","of","at","by","for","with","about","against","between","into","through","during","before","after","above","below","to","from","up","down","in","out","on","off","over","under","again","further","then","once","here","there","when","where","why","how","all","any","both","each","few","more","most","other","some","such","no","nor","not","only","own","same","so","than","too","very","s","t","can","will","just","don","should","now"]
	function tokenize(s: string): string[] {
		let tokens = s
			.split(/\s+/) // split at whitespace
			.map((s) => s.toLowerCase().replace(/[.,!?]$/, "")) // remove ending punctuation
			.filter((token) => !stopWords.includes(token)) // remove stop words
			.filter((token) => token.length > 1);
		return tokens;
	}

	function render(myself: UserBagOfWords, other: UserBagOfWords, limit = 20) {
		// css gradient
		// https://www.eggradients.com/?73afe765_page=4
		let template = `<div id='message-history'>
	<header>your message history with ${other.displayName}</header>
	<section>
	<div class='name'>${myself.displayName}</div>
	${myself.bagOfWords
		.slice(0, limit)
		.map((w) => `<div>${w.word} (${w.count})</div>`)
		.join("")}
	</section>
	<section>
	<div  class='name'>
	${other.displayName}
	</div>
	${other.bagOfWords
		.slice(0, limit)
		.map((w) => `<div>${w.word} (${w.count})</div>`)
		.join("")}
	</section>
	</div>
	<style>
	#message-history {
	background-color: #537895;
	background-image: linear-gradient(315deg, #537895 0%, #09203f 74%);
	color: rgba(255,255,255,0.85);
	font-family: Helvetica;
	font-size: 16px;
	text-align: center;
	display: grid;
	grid-template-columns: 1fr 1fr;
	grid-gap: 12px;
	width: 600px;
	padding: 12px;
	font-weight: 800;
	}
	#message-history * {
	font-size: inherit;
	}
	#message-history header {
	font-size: 32px;
	grid-column: 1 / span all;
	margin-bottom: 24px;
	}
	#message-history header {
	flex-basis: 100%;
	}
	#message-history .name {
	margin-bottom: 1rem;
	}
	</style>`;
		let div = document.createElement("div");
		div.innerHTML = template;
		return div;
	}

	async function main() {
		const numCurrent = document.querySelectorAll(messageSelector).length;
		const numRest = (maxPage - 1) * 10;
		const numExpected = numCurrent + numRest;
		const myself = document.querySelector("#globaluserlink").textContent;
		const other = document.querySelector("#content > h1 > a").textContent;
		const target = document.querySelector(".forumthread");
		let loader = document.createElement("progress");
		target.insertBefore(loader, target.firstChild);
		loader.value = 0;
		loader.max = numExpected;
		document.addEventListener($update, (e: CustomEvent) => {
			loader.value = loader.value + e.detail;
		});
		const repo = new MessageRepository(myself, other);
		const msgs = await repo.get();
		loader.remove();
		const bags = processMessages(msgs, tokenize);
		let findBag = (name) => bags.find((e) => e.displayName === name);
		const div = render(findBag(myself), findBag(other));
		target.insertBefore(div, target.firstChild);
	}

	async function ready() {
		const target = document.querySelector(".forumthread");
		let button = document.createElement("button");
		button.textContent = "Get message history";
		button.addEventListener("click", () => {
			button.remove();
			main();
		});
		target.insertBefore(button, target.firstChild);
	}

	ready();
})();
