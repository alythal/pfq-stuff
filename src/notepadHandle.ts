/**
 * Parse the HTML string to a Document
 * @param text HTML string
 */
function parse(text: string): Document {
	let doc = new DOMParser().parseFromString(text, "text/html");
	return doc;
}

/**
 * Class to manage a file in the the PFQ Notepad. Currently only
 * supports files in the root directory
 *
 * let handle = await NotepadHandle.createHandle("boop")
 * handle.read()                // returns the currentfile content
 * handle.write('hello world') // write to the file
 */
export default class NotepadHandle {
	public id: string = "";

	private constructor(public filename: string) {}

	/**
	 * Returns a handle for the file in this notepad.
	 * Creates the file if it does not exist.
	 *
	 * @param filename Name of the file
	 */
	static async createHandle(filename: string): Promise<NotepadHandle> {
		let handle = new NotepadHandle(filename);
		await handle.init();
		return handle;
	}

	// Initialise - assign the ID for the file which is used in fetch
	// requests
	async init() {
		let id = await this.getId();
		if (id) {
			this.id = id;
		} else {
			this.id = await this.create();
		}
	}

	/**
	 * Get the ID for the file this handle deals with.
	 *
	 * @returns The ID of the file if it exists
	 * @returns null if it does not
	 */
	async getId(): Promise<string | null> {
		// Fetch the notepad page
		let res = await fetch("https://pokefarm.com/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: '{"directory":null}',
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		// Finds the element matching the tag below and extracts data-file
		// <a data-file='12345'>this.filename</a>
		let file = Array.from(doc.querySelectorAll("a")).filter(
			(e) => e.textContent === this.filename
		);
		return file.pop()?.dataset?.file ?? null;
	}

	/**
	 * Creates the file and returns the ID of the newly created file
	 */
	async create(): Promise<string> {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				directory: "",
				mode: "newfile",
				save: {
					name: this.filename,
				},
			}),
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		let id = doc.querySelector("form")?.dataset?.fileform;
		if (id) {
			return id;
		}
		throw new Error(`Failed to create notepad file`);
	}

	/**
	 * Reads the file content as a string
	 */
	async read(): Promise<string> {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				directory: null,
				file: this.id,
			}),
			method: "POST",
		});
		let doc = parse((await res.json()).html);
		let value = doc.querySelector<HTMLTextAreaElement>(
			`textarea[name='content']`
		)?.value;
		if (value) {
			return value;
		}
		throw new Error(`Failed to create notepad file`);
	}

	/**
	 * Write content to file
	 * @param content The content to write
	 */
	async write(content: string) {
		let res = await fetch("/farm/notepad", {
			headers: {
				"X-Requested-With": "Love",
			},
			body: JSON.stringify({
				file: this.id,
				mode: "save",
				save: {
					content,
					name: this.filename,
				},
			}),
			method: "POST",
		});
		if (res.status !== 200) {
			throw Error(`Could not save to notepad file`);
		}
	}
}
