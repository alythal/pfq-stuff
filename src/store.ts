import NotepadHandle from "./notepadHandle";

declare global {
	interface Window {
		$: any;
	}
}

export interface State {
	version: number;
}

export default class Store<T extends State> {
	/**
	 * Create a store to contain a config for your userscript. The config should be
	 * a single serializable JSON object. localStorage is used to cache the store,
	 * to avoid making a fetch on every read. Every write does create a fetch.
	 *
	 * const store = await Store.createStore("my-amazing-script", { myScriptValue: 1})
	 * await store.set('myScriptValue', 42) // saved to PFQ notepad
	 * await store.get('myScriptValue') // 42
	 *
	 * If you open the PFQ notepad you'll find a new file with the content
	 * {myScriptValue:42}
	 *
	 * @param {string} userScriptName
	 * @param {Object} initialState
	 */
	static async createStore<U extends State>(
		userScriptName: string,
		initialState: U
	): Promise<Store<U>> {
		const storageName = `${window.$.USERID}-${userScriptName}`;
		const handle = await NotepadHandle.createHandle(storageName);
		let store = new Store<U>(handle, storageName);
		await store.init(initialState);
		return store;
	}

	constructor(public handle: NotepadHandle, public storageName: string) {}

	cacheExists(): boolean {
		return localStorage.getItem(this.storageName) !== null;
	}

	stringify(state: T): string {
		return JSON.stringify(state);
	}

	parse(s: string): T {
		return JSON.parse(s) as T;
	}

	// ensure the state is in localStorage
	async init(initialState: T) {
		const updateIfNeeded = async () => {
			let state = this.get();
			if (state.version < initialState.version) {
				let merged = {
					...initialState,
					...state,
					version: initialState.version,
				};
				await this.set(merged);
			}
		};
		// localstorage exists - update if needed
		if (this.cacheExists()) {
			await updateIfNeeded();
			return;
		}
		// localstorage does not exist but file exists
		let read = await this.handle.read();
		if (read) {
			await this._setString(read);
			await updateIfNeeded();
			return;
		}
		// file does not exist
		let s = JSON.stringify(initialState);
		this.handle.write(s);
		this._setString(s);
	}

	// get the state synchronously from localStorage
	get(): T {
		return this.parse(localStorage.getItem(this.storageName) as string);
	}

	// set to localstorage (sync) + notepad (async)
	// the function is guaranteed to return after localstorage is updated
	async set(state: T) {
		let s = this.stringify(state);
		return await this._setString(s);
	}

	async _setString(s: string) {
		localStorage.setItem(this.storageName, s);
		return await this.handle.write(s);
	}
}
